import axios from 'axios';
import to from 'await-to-js';

interface ApiResponseSchema {
  results: any
}

interface ApiEndpointSchema {
  get(params?: object): Promise<ApiResponseSchema>,
}

const httpClient = {
  users: {
    async get(params?: object): Promise<ApiResponseSchema> {
      const [err, res] = await to(axios.get('https://randomuser.me/api/', { params }));
      if(err) return Promise.reject({ data: null, error: err });
      return Promise.resolve({ results: res.data.results })
    },
  } as ApiEndpointSchema
};

export default httpClient;
