import React, { useCallback, useState } from 'react';
import { LabelText } from '@atomic-design/atoms';
import { UserRow, SearchBar } from '@atomic-design/molecules';
import { httpClient }  from '@atomic-design/http-client';

export const SearchUsersList = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchUsers = useCallback(async (count = 5): Promise<void> => {
    setLoading(true);
    const data = await httpClient.users.get({ results: count });
    if(data){
      setUsers(data.results);
    }
    setLoading(false);
  }, []);

  const handleSearch = useCallback((text = ''): void => {
    if(!text.length){
      return setUsers([]);
    }
    fetchUsers(text.length).then();
  }, [fetchUsers]);

  return (
    <div className="inline-block bg-gray-100 p-10 w-2/3">
      <LabelText
        className="mb-5 uppercase font-bold"
        color="text-orange"
        size="text-xl"
      >
        Users List
      </LabelText>
      <div className="mb-4">
        <SearchBar delayChange onAccept={handleSearch} />
      </div>
      <div className="flex flex-col">
        {users.map((v, i) => (
          <UserRow key={i} loading={loading} username={v.login.username} email={v.email}  avatarUrl={v.picture.medium} />
        ))}
        {!users.length && !loading && <p className="text-gray-600">Find user</p>}
        {!users.length && loading && <p className="text-gray-600">Loading...</p>}
      </div>
    </div>
  );
};

export default SearchUsersList;
