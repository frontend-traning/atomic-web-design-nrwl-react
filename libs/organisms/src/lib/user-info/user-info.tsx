import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { Avatar, LabelText } from '@atomic-design/atoms';

export interface UserDataInterface {
  name: {
    title: string,
    first: string,
    last: string
  },
  picture: {
    large: string
  }
}

export const UserInfo = () => {
  const initData: UserDataInterface = {
    name: { first: 'Loading', last: '', title: '' },
    picture: { large: '/assets/placeholder.svg' }
  };
  const [user, setUser] = useState(initData);

  const fetchUser = useCallback(async () => {
    const res = await axios.get(`https://randomuser.me/api/?results=1`);
    if(res.data){
      setUser(res.data.results[0]);
    }
  }, []);

  useEffect(() => {
    fetchUser().then();
  }, [fetchUser]);

  return (
    <div className="inline-block bg-gray-100 p-5 rounded-lg">
      <div className="flex flex-col justify-center items-center">
        <Avatar src={user.picture.large} />
        <div className="flex items-end mt-5">
          <LabelText color="text-orange" size="text-sm">{user.name.title}</LabelText>
          <LabelText color="text-black" size="text-sm" className="mx-1">{user.name.first}</LabelText>
          <LabelText color="text-black" size="text-sm">{user.name.last}</LabelText>
        </div>
      </div>
    </div>
  );
};

export default UserInfo;
