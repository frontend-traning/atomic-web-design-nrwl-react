import React from 'react';

export interface TextFieldProps {
  className?: string,
  value?: string,
  placeholder?: string,
  onChange: Function,
  onEnterKey?: Function,
}

export const TextField = (props: TextFieldProps) => {
  return (
    <input
      value={props.value}
      onChange={(e) => {
        return props.onChange(e.target.value || '');
      }}
      onKeyDown={(e) => props.onEnterKey ? e.key === 'Enter' ?  props.onEnterKey() : null : null}
      type="search"
      name="search"
      placeholder={props.placeholder || 'Placeholder'}
      className={"bg-white h-10 px-5 pr-10 rounded-lg text-sm focus:outline-none border border-orange " + props.className}
    />
  );
};

export default TextField;
