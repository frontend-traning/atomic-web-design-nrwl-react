import React from 'react';

export interface ButtonProps {
  onClick: Function,
  className?: string,
  children?: Function | string
}

export const Button = (props: ButtonProps) => {
  return (
    <button
      onClick={() => props.onClick()}
      type="button"
      className={"border-none focus:outline-none border rounded-lg hover:bg-active bg-orange font-md text-white px-5 py-2 " + props.className}
    >
      {props.children || 'Button'}
    </button>
  );
};

export default Button;
