import React  from 'react';

export interface LabelProps {
  className?: string,
  children?: Function | string | number,
  color: 'text-black' | 'text-orange' | string,
  size: 'text-sm' | 'text-md' | 'text-lg' | 'text-xl' | 'text-2xl' | 'text-3xl' | 'text-4xl',
}

export const LabelText = (props: LabelProps) => {
  return (
    <div className={`text-md ${props.color} ${props.size || ''} ${props.className}`}>{props.children}</div>
  );
};

export default LabelText;
