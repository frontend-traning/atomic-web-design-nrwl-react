import React  from 'react';

export interface AvatarProps {
  className?: string,
  src?: string
}

export const Avatar = (props: AvatarProps) => {

  const fallback = '/assets/placeholder.svg';

  return (
    <img className={"inline-block object-cover bg-gray-200 rounded-full w-10 h-10 border-none border-orange"+ props.className} src={props.src || fallback} alt="avatar" />
  );
};

export default Avatar;
