import React from 'react';
import { Avatar, LabelText } from '@atomic-design/atoms';

export interface UserRowProps {
  avatarUrl?: string,
  username?: string,
  email?: string,
  loading?: boolean
}

export const UserRow = (props: UserRowProps) => {
  return (
    <div className="mb-2 w-full">
      <div className="flex items-center bg-white px-6 py-2 rounded-lg shadow-lg text-center">
        <div className="mr-5">
          <Avatar src={!props.loading ? props.avatarUrl : '/assets/placeholder.svg'} />
        </div>
        <div className="flex flex-col justify-start items-start h-full">
          {!props.loading && (
            <LabelText size="text-lg" color="text-gray-700" className="font-medium">
              {props.username || 'Username'}
            </LabelText>
          )}
          {props.loading && <div className="bg-gray-100 w-40 h-6 mb-3" />}
          {!props.loading && (
            <LabelText color="text-orange" size="text-md" className="block">
              {props.email || 'useremail@ex.com'}
            </LabelText>
          )}
          {props.loading && <div className="bg-gray-100 w-full h-5" />}
        </div>
      </div>
    </div>
  );
};

export default UserRow;
