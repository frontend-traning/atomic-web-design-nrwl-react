import React, { useState, useCallback } from 'react';
import { Button, TextField } from '@atomic-design/atoms';
import { debounce } from 'lodash';

export interface SearchBarProps {
  onAccept: Function,
  delayChange?: boolean
}

const debounced = debounce((action, value) => {
  return action(value);
}, 300);

export const SearchBar = (props: SearchBarProps) => {
  const [text, setText] = useState('');

  const handleAccept = useCallback(() => {
    props.onAccept(text);
  }, [props, text]);

  const handleChange = useCallback((value) => {
    setText(value);
    if(props.delayChange){
      debounced(props.onAccept, value)
    }

  }, [props]);

  return (
    <div className="flex items-center">
      <Button onClick={handleAccept} className="h-10 outline-none shadow border-r-0 rounded-tr-none rounded-br-none">
        Search
      </Button>
      <TextField placeholder="Type..." value={text} onChange={handleChange} onEnterKey={handleAccept} className="h-10 w-full shadow border-l-0 rounded-tl-none rounded-bl-none" />
    </div>
  );
};

export default SearchBar;
