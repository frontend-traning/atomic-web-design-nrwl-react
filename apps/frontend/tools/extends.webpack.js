// eslint-disable-next-line @typescript-eslint/no-var-requires
const getBabelWebpackConfig = require('@nrwl/react/plugins/babel');

module.exports = (config, context) => {
  const co = getBabelWebpackConfig(config);
  const newRules = co.module.rules.map(v => {
    if(!v.use) return v;
    const a = v.use.find(v => v.loader === 'postcss-loader');
    a.options.plugins = [
      a.options.plugins,
      require('tailwindcss'),
      require('autoprefixer'),
    ];
    return {...v}
  });

  newRules.push({
    test: /\.svg$/,
    use: ['@svgr/webpack', 'url-loader']
  });

  return {
    ...co,
    module: {
      ...co.module,
      rules: newRules
    },
  };
};


// "webpackConfig": "@nrwl/react/plugins/babel"
