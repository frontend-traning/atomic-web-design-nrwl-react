import React from 'react';
import { Button, TextField, Avatar, LabelText } from '@atomic-design/atoms';

function Atoms() {
  return (
    <div className="my-10">
      <h2 className="text-4xl text-orange uppercase font-bold border-b border-orange">
        Atoms
      </h2>
      <div className="flex flex-col">
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            LabelText:
          </div>
          <div className="text-lg w-10/12">
            <LabelText color="text-orange" size="text-3xl">Hello world!</LabelText>
          </div>
        </div>
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            Button:
          </div>
          <div className="text-lg w-10/12">
            <Button onClick={() => null} />
          </div>
        </div>
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            TextField:
          </div>
          <div className="text-lg w-10/12">
            <TextField onChange={() => null} />
          </div>
        </div>
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            Avatar:
          </div>
          <div className="text-lg w-10/12">
            <Avatar />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Atoms;
