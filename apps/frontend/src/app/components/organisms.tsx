import React from 'react';
import { SearchUsersList, UserInfo } from '@atomic-design/organisms';

function Organisms() {
  return (
    <div className="my-10">
      <h2 className="text-4xl text-orange uppercase font-bold border-b border-orange">
        Organisms
      </h2>
      <div className="flex flex-col">
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            UserInfo:
          </div>
          <div className="text-lg w-10/12">
            <UserInfo />
          </div>
        </div>
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            SearchUsersList:
          </div>
          <div className="text-lg w-10/12">
            <SearchUsersList />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Organisms;
