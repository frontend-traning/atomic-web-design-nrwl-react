import React from 'react';
import { SearchBar, UserRow } from '@atomic-design/molecules';

function Molecules() {
  return (
    <div className="my-10">
      <h2 className="text-4xl text-orange uppercase font-bold border-b border-orange">
        Molecules
      </h2>
      <div className="flex flex-col">
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            SearchBar:
          </div>
          <div className="text-lg w-10/12">
            <SearchBar onAccept={() => null} />
          </div>
        </div>
        <div className="flex my-5">
          <div className="text-lg w-2/12">
            UserRow:
          </div>
          <div className="text-lg w-10/12">
            <UserRow />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Molecules;
