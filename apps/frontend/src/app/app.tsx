import React from 'react';
import Atoms from './components/atoms';
import Molecules from './components/molecules';
import Organisms from './components/organisms';

export const App = () => {

  return (
    <div className="container mx-auto">
      <div className="flex justify-center font-bold text-5xl">
        <span className="text-orange mx-2">ATOMIC</span> WEB DESIGN
      </div>
      <Atoms />
      <Molecules />
      <Organisms />
    </div>
  )
};

export default App;
